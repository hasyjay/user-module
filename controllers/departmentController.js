var Department = require('../models/department');
var models = require('../models');


// Display department create form on GET.
exports.department_create_get = function(req, res, next) {
        // create department GET controller logic here 
        res.render('forms/department_form', { title: 'Create department', layout: 'layouts/detail' });
};

// Handle department create on POST.
exports.department_create_post = function(req, res, next) {
        // create department POST controller logic here
        models.Department.create({
                departmentName: req.body.departmentName

        }).then(function() {
                res.redirect('/hassan/departments')
        })
};

// Display department delete form on GET.
exports.department_delete_get = function(req, res, next) {
        // GET logic to delete an department here
        if (!req.params.department_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Department.destroy({
                        // find the post_id to delete from database
                        where: {
                                id: req.params.department_id
                        }
                }).then(function() {
                        // If an department gets deleted successfully, we just redirect to posts list
                        // no need to render a page
                        res.redirect('/hassan/departments');
                        console.log("department deleted successfully");
                });
        }



};

// Handle department delete on POST.
exports.department_delete_post = function(req, res, next) {
        // GET logic to delete an department here
        models.department.destroy({
                // find the post_id to delete from database
                where: {
                        id: req.params.department_id
                }
        }).then(function() {
                // If an department gets deleted successfully, we just redirect to posts list
                // no need to render a page
                res.redirect('/hassan/departments');
                console.log("department deleted successfully");
        });


};

// Display department update form on GET.
exports.department_update_get = function(req, res, next) {
        // GET logic to update an department here
        if (!req.params.department_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Department.findById(
                        req.params.department_id
                ).then(function(department) {
                        // renders a post form
                        res.render('forms/department_form', { title: 'Update department', department: department, layout: 'layouts/detail' });
                        console.log("department update get successful");
                });
        }


        // renders department form

};

// Handle post update on POST.
exports.department_update_post = function(req, res, next) {
        // POST logic to update an department here
        models.Department.update({
                        departmentName: req.body.departmentName
                }, {
                        where: {
                                id: req.params.department_id
                        }
                }

        ).then(function() {
                res.redirect('/hassan/departments')
        })

};

// Display list of all departments.
exports.department_list = function(req, res, next) {
        // GET controller logic to list all departments
        models.Department.findAll().then((departments) => {

                res.render('pages/department_list', { title: 'department List', layout: 'layouts/list', departments: departments });
        })


};

// Display detail page for a specific department.
exports.department_detail = async function(req, res, next) {
        // GET controller logic to display just one department

        if (!req.params.department_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                // const departments = await models.Department.findAll();

                models.Department.findById(
                        req.params.department_id
                        // , {
                        //         include: [

                        //                 {
                        //                         model: models.User
                        //                 }

                        //         ]
                        // }
                ).then(function(department) {
                        // renders an inividual department details page
                        res.render('pages/department_detail', { title: 'department Details', layout: 'layouts/detail', department: department });

                });
        }

        // renders an individual department details page

};
