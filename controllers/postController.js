var Post = require('../models/post');
var models = require('../models');
var Department = require("../models/department")
var Role = require("../models/role")
var Profile = require("../models/profile")
var User = require('../models/user')

// Display post create form on GET.
exports.post_create_get = async function(req, res, next) {
    // create post GET controller logic here 
    var profiles = await models.Profile.findAll();
    var users = await models.User.findAll();
    var departments = await models.Department.findAll();
    res.render('forms/post_form', { title: 'Create post', layout: 'layouts/detail', users: users, departments: departments, profiles: profiles });
};

// Handle post create on POST.
exports.post_create_post = async function(req, res, next) {
    // create post POST controller logic here
    models.Post.create({
        postTitle: req.body.postTitle,
        postBody: req.body.postBody,
        UserId: req.body.user_id,
        DepartmentId: req.body.user_id,
        ProfileId: req.body.profile_id
    }).then(function() {
        res.redirect('/hassan/posts')
    })
};

// Display post delete form on GET.
exports.post_delete_get = function(req, res, next) {
    // GET logic to delete an post here
    if (!req.params.post_id.match(/^[0-9]+$/)) {
        res.render('error/post_error')
    }
    else {
        models.Post.destroy({
            // find the post_id to delete from database
            where: {
                id: req.params.post_id
            }
        }).then(function() {
            // If an post gets deleted successfully, we just redirect to posts list
            // no need to render a page
            res.redirect('/hassan/posts');
            console.log("post deleted successfully");
        });
    }



};

// Handle post delete on POST.
exports.post_delete_post = function(req, res, next) {
    // GET logic to delete an post here
    models.Post.destroy({
        // find the post_id to delete from database
        where: {
            id: req.params.post_id
        }
    }).then(function() {
        // If an post gets deleted successfully, we just redirect to posts list
        // no need to render a page
        res.redirect('/hassan/posts');
        console.log("post deleted successfully");
    });


};

// Display post update form on GET.
exports.post_update_get = function(req, res, next) {
    // GET logic to update an post here
    if (!req.params.post_id.match(/^[0-9]+$/)) {
        res.render('error/post_error')
    }
    else {
        models.Post.findById(
            req.params.post_id
        ).then(function(post) {
            // renders a post form
            res.render('forms/post_form', { title: 'Update post', post: post, layout: 'layouts/detail' });
            console.log("post update get successful");
        });
    }


    // renders post form

};

// Handle post update on POST.
exports.post_update_post = function(req, res, next) {
    // POST logic to update an post here
    models.Post.update({
            postTitle: req.body.postTitle,
            postBody: req.body.postBody
        }, {
            where: {
                id: req.params.post_id
            }
        }

    ).then(function() {
        res.redirect('/hassan/posts')
    })

};

// Display list of all posts.
exports.post_list = function(req, res, next) {
    // GET controller logic to list all posts
    models.Post.findAll().then((posts) => {

        res.render('pages/post_list', { title: 'post List', layout: 'layouts/list', posts: posts });
    })


};

// Display detail page for a specific post.
exports.post_detail = async function(req, res, next) {
    // GET controller logic to display just one post

    if (!req.params.post_id.match(/^[0-9]+$/)) {
        res.render('error/post_error')
    }
    else {
        // const categories = await models.Category.findAll();

        models.Post.findById(
            req.params.post_id, {
                include: [

                    {
                        model: models.User
                    }

                ]
            }
        ).then(function(post) {
            // renders an inividual post details page
            res.render('pages/post_detail', { title: 'post Details', layout: 'layouts/detail', post: post });

        });
    }

    // renders an individual post details page

};

exports.user_post_dept = async function(req, res) {

    await models.Post.findAll({
            where: {
                UserId: req.params.user_id,
                DepartmentId: req.params.department_id


            }
        }

    ).then(function(posts) {

        // res.json({
        //     data: post
        //})
        res.render('pages/user_post_dept', { title: 'user post dept', layout: 'layouts/detail', posts: posts });
    })
};



exports.user_post_dept_profile = async function(req, res) {

    await models.Post.findAll({
            where: {

                UserId: req.params.user_id,
                DepartmentId: req.params.department_id,
                ProfileId: req.params.profile_id
            }
        }

    ).then(function(posts) {

        // res.json({
        //     data: posts
        // })

        res.render('pages/user_post_dept_profile', { title: 'user post dept profile', layout: 'layouts/detail', posts: posts });
    })
}


exports.index = function(req, res) {

    // find the count of posts in database
    models.Post.findAndCountAll().then(function(postCount) {


        // find the count of users in database
        models.Department.findAndCountAll().then(function(departmentCount) {

            // find the count of comments in database
            models.Role.findAndCountAll().then(function(roleCount) {
                // find the count of categories in database
                models.Profile.findAndCountAll().then(function(profileCount) {

                    res.render('pages/index', {
                        title: 'Homepage',
                        postCount: postCount,
                        departmentCount: departmentCount,
                        roleCount: roleCount,
                        profileCount: profileCount,
                        layout: 'layouts/main'
                    });

                    // res.render('pages/index_list_sample', { title: 'post Details', layout: 'layouts/list'});
                    // res.render('pages/index_detail_sample', { page: 'Home' , title: 'post Details', layout: 'layouts/detail'});

                })
            })

        })

    })

};
