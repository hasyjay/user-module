var Profile = require('../models/profile');
var models = require('../models');


// Display profile create form on GET.
exports.profile_create_get = function(req, res, next) {
        // create profile GET controller logic here 
        res.render('forms/profile_form', { title: 'Create profile', layout: 'layouts/detail' });
};

// Handle profile create on POST.
exports.profile_create_post = function(req, res, next) {
        // create profile POST controller logic here
        models.Profile.create({
                profileName: req.body.profileName

        }).then(function() {
                res.redirect('/hassan/profiles')
        })
};

// Display profile delete form on GET.
exports.profile_delete_get = function(req, res, next) {
        // GET logic to delete an profile here
        if (!req.params.profile_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Profile.destroy({
                        // find the post_id to delete from database
                        where: {
                                id: req.params.profile_id
                        }
                }).then(function() {
                        // If an profile gets deleted successfully, we just redirect to posts list
                        // no need to render a page
                        res.redirect('/hassan/profiles');
                        console.log("profile deleted successfully");
                });
        }



};

// Handle profile delete on POST.
exports.profile_delete_post = function(req, res, next) {
        // GET logic to delete an profile here
        models.Profile.destroy({
                // find the post_id to delete from database
                where: {
                        id: req.params.profile_id
                }
        }).then(function() {
                // If an profile gets deleted successfully, we just redirect to posts list
                // no need to render a page
                res.redirect('/hassan/profiles');
                console.log("profile deleted successfully");
        });


};

// Display profile update form on GET.
exports.profile_update_get = function(req, res, next) {
        // GET logic to update an profile here
        if (!req.params.profile_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Profile.findById(
                        req.params.profile_id
                ).then(function(profile) {
                        // renders a post form
                        res.render('forms/profile_form', { title: 'Update profile', profile: profile, layout: 'layouts/detail' });
                        console.log("profile update get successful");
                });
        }


        // renders profile form

};

// Handle post update on POST.
exports.profile_update_post = function(req, res, next) {
        // POST logic to update an profile here
        models.Profile.update({
                        profileName: req.body.profileName
                }, {
                        where: {
                                id: req.params.profile_id
                        }
                }

        ).then(function() {
                res.redirect('/hassan/profiles')
        })

};

// Display list of all profiles.
exports.profile_list = function(req, res, next) {
        // GET controller logic to list all profiles
        models.Profile.findAll().then((profiles) => {

                res.render('pages/profile_list', { title: 'profile List', layout: 'layouts/list', profiles: profiles });
        })


};

// Display detail page for a specific profile.
exports.profile_detail = async function(req, res, next) {
        // GET controller logic to display just one profile

        if (!req.params.profile_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                // const profiles = await models.Profile.findAll();

                models.Profile.findById(
                        req.params.profile_id

                        // , {
                        //         include: [

                        //                 {
                        //                         model: models.User
                        //                 }

                        //         ]
                        // }
                ).then(function(profile) {
                        // renders an inividual profile details page
                        res.render('pages/profile_detail', { title: 'profile Details', layout: 'layouts/detail', profile: profile });

                });
        }

        // renders an individual profile details page

};
