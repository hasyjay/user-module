var Role = require('../models/role');
var models = require('../models');


// Display role create form on GET.
exports.role_create_get = function(req, res, next) {
        // create role GET controller logic here 
        res.render('forms/role_form', { title: 'Create role', layout: 'layouts/detail' });
};

// Handle role create on POST.
exports.role_create_post = function(req, res, next) {
        // create role POST controller logic here
        models.Role.create({
                roleName: req.body.roleName,

        }).then(function() {
                res.redirect('/hassan/roles')
        })
};

// Display role delete form on GET.
exports.role_delete_get = function(req, res, next) {
        // GET logic to delete an role here
        if (!req.params.role_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Role.destroy({
                        // find the post_id to delete from database
                        where: {
                                id: req.params.role_id
                        }
                }).then(function() {
                        // If an role gets deleted successfully, we just redirect to posts list
                        // no need to render a page
                        res.redirect('/hassan/roles');
                        console.log("role deleted successfully");
                });
        }



};

// Handle role delete on POST.
exports.role_delete_post = function(req, res, next) {
        // GET logic to delete an role here
        models.Role.destroy({
                // find the post_id to delete from database
                where: {
                        id: req.params.role_id
                }
        }).then(function() {
                // If an role gets deleted successfully, we just redirect to posts list
                // no need to render a page
                res.redirect('/hassan/roles');
                console.log("role deleted successfully");
        });


};

// Display role update form on GET.
exports.role_update_get = function(req, res, next) {
        // GET logic to update an role here
        if (!req.params.role_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Role.findById(
                        req.params.role_id
                ).then(function(role) {
                        // renders a post form
                        res.render('forms/role_form', { title: 'Update role', role: role, layout: 'layouts/detail' });
                        console.log("role update get successful");
                });
        }


        // renders role form

};

// Handle post update on POST.
exports.role_update_post = function(req, res, next) {
        // POST logic to update an role here
        models.Role.update({
                        roleName: req.body.roleName
                }, {
                        where: {
                                id: req.params.role_id
                        }
                }

        ).then(function() {
                res.redirect('/hassan/roles')
        })

};

// Display list of all roles.
exports.role_list = function(req, res, next) {
        // GET controller logic to list all roles
        models.Role.findAll().then((roles) => {

                res.render('pages/role_list', { title: 'role List', layout: 'layouts/list', roles: roles });
        })


};

// Display detail page for a specific role.
exports.role_detail = async function(req, res, next) {
        // GET controller logic to display just one role

        if (!req.params.role_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                // const roles = await models.Role.findAll();

                models.Role.findById(
                        req.params.role_id

                        // {
                        //         include: [

                        //                 {
                        //                         model: models.User
                        //                 }

                        //         ]
                        // }
                ).then(function(role) {
                        // renders an inividual role details page
                        res.render('pages/role_detail', { title: 'role Details', layout: 'layouts/detail', role: role });

                });
        }

        // renders an individual role details page

};
