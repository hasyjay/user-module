var User = require('../models/user');
var models = require('../models');
var Profile = require("../models/profile")
var Role = require('../models/role')
var Post = require("../models/post")
var Department = require('../models/department')

// Display user create form on GET.
exports.user_create_get = async function(req, res, next) {
        // create user GET controller logic here 

        var profiles = await models.Profile.findAll()
        var roles = await models.Role.findAll()
        var departments = await models.Department.findAll()
        res.render('forms/user_form', { title: 'Create user', layout: 'layouts/detail', profiles: profiles, roles: roles, departments: departments });
};

// Handle user create on POST.
exports.user_create_post = async function(req, res, next) {
        // create user POST controller logic here
        models.User.create({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                username: req.body.username,
                password: req.body.password,
                email: req.body.email,
                current_business: req.body.current_business,
                permission: req.body.permission,
                ProfileId: req.body.profile_id,
                RoleId: req.body.role_id,
                DepartmentId: req.body.department_id
                // roleID: req.body.roleID,
                // profileID: req.body.profileID,
                // departmentID: req.body.departmentID
        }).then(function() {
                res.redirect('/hassan/users')
        })
};

// Display user delete form on GET.
exports.user_delete_get = function(req, res, next) {
        // GET logic to delete an user here
        if (!req.params.user_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.User.destroy({
                        // find the post_id to delete from database
                        where: {
                                id: req.params.user_id
                        }
                }).then(function() {
                        // If an user gets deleted successfully, we just redirect to posts list
                        // no need to render a page
                        res.redirect('/hassan/users');
                        console.log("user deleted successfully");
                });
        }



};

// Handle user delete on POST.
exports.user_delete_post = function(req, res, next) {
        // GET logic to delete an user here
        models.User.destroy({
                // find the post_id to delete from database
                where: {
                        id: req.params.user_id
                }
        }).then(function() {
                // If an user gets deleted successfully, we just redirect to posts list
                // no need to render a page
                res.redirect('/hassan/users');
                console.log("user deleted successfully");
        });


};

// Display user update form on GET.
exports.user_update_get = function(req, res, next) {
        // GET logic to update an user here
        if (!req.params.user_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.User.findById(
                        req.params.user_id
                ).then(function(user) {
                        // renders a post form
                        res.render('forms/user_form', { title: 'Update user', user: user, layout: 'layouts/detail' });
                        console.log("user update get successful");
                });
        }


        // renders user form

};

// Handle post update on POST.
exports.user_update_post = function(req, res, next) {
        // POST logic to update an user here
        models.User.update({
                        firstName: req.body.firstName,
                        lastName: req.body.lastName,
                        username: req.body.username,
                        password: req.body.password,
                        email: req.body.email,
                        current_business: req.body.current_business,
                        permission: req.body.permission
                }, {
                        where: {
                                id: req.params.user_id
                        }
                }

        ).then(function() {
                res.redirect('/hassan/users')
        })

};

// Display list of all users.
exports.user_list = function(req, res, next) {
        // GET controller logic to list all users
        models.User.findAll().then((users) => {

                res.render('pages/user_list', { title: 'user List', layout: 'layouts/list', users: users });
        })


};

// Display detail page for a specific user.
exports.user_detail = async function(req, res, next) {
        // GET controller logic to display just one user

        if (!req.params.user_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                // const categories = await models.Category.findAll();

                models.User.findById(
                        req.params.user_id, {
                                include: [

                                        {
                                                model: models.Profile,
                                                attributes: ['id', 'profileName']
                                        },
                                        {
                                                model: models.Role,
                                                attributes: ['id', 'roleName']
                                        },
                                        {
                                                model: models.Department,
                                                attributes: ['id', 'departmentName']
                                        },

                                        {
                                                model: models.Post

                                        },

                                ]
                        }
                ).then(function(user) {
                        // renders an inividual user details page
                        res.render('pages/user_detail', { title: 'user Details', layout: 'layouts/detail', user: user });

                });
        }

        // renders an individual user details page

};



exports.user_post = async function(req, res, next) {


        models.User.findById(req.params.user_id, {
                        include: [

                                {
                                        model: models.Post
                                }


                        ]
                }

        ).then(function(user, err) {
                res.render('pages/user_post', { title: "single user posts", layout: 'layouts/detail', user: user, err: err })
        })

}
