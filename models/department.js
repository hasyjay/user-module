module.exports = function(sequelize, Sequelize) {

    var Department = sequelize.define('Department', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        departmentName: {
            type: Sequelize.STRING,
            allowNull: false
        },





    });

    // Department.associate = ((models) => {
    //     models.Department.hasMany(models.User, {
    //         onDelete: "CASCADE",
    //         foreignKey: 'departmentID'
    //     })

    // })

    Department.associate = function(models) {
        models.Department.hasMany(models.User, {
            onDelete: "CASCADE",
            foreignKey: 'DepartmentId'
        });

        models.Department.hasMany(models.Post, {
            onDelete: "CASCADE",
            foreignKey: "DepartmentId"
        })


        //   models.post.belongsToMany(models.Category, {
        //     as: 'categories',
        //     through: 'postCategories',
        //     foreignKey: 'post_id'
        //   });
        //   models.post.hasMany(models.Comment);



    };

    return Department;

}
