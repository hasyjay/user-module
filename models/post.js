'use strict';
module.exports = (sequelize, DataTypes) => {
  var Post = sequelize.define('Post', {
    postTitle: DataTypes.STRING,
    postBody: DataTypes.STRING,
    UserId: DataTypes.INTEGER,
    DepartmentID: DataTypes.INTEGER

    // AuthorId: DataTypes.INTEGER
  });

  Post.associate = function(models) {
    models.Post.belongsTo(models.User, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
    models.Post.belongsTo(models.Department, {
      onDelete: "CASCADE"
    })

    models.Post.belongsTo(models.Profile, {
      onDelete: 'CASCADE'
    })

    //   models.post.belongsToMany(models.Category, {
    //     as: 'categories',
    //     through: 'postCategories',
    //     foreignKey: 'post_id'
    //   });
    //   models.post.hasMany(models.Comment);



  };
  return Post;
};

// Make sure you complete other models fields
