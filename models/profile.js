module.exports = function(sequelize, Sequelize) {

    var Profile = sequelize.define('Profile', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        profileName: {
            type: Sequelize.STRING,
            allowNull: false
        },





    });



    Profile.associate = function(models) {
        models.Profile.hasMany(models.User, {
            onDelete: "CASCADE",
            foreignKey: 'ProfileId'
        });

        models.Profile.hasMany(models.Post, {
            onDelete: "CASCADE",
            foreignKey: "ProfileId"
        })


        //   models.post.belongsToMany(models.Category, {
        //     as: 'categories',
        //     through: 'postCategories',
        //     foreignKey: 'post_id'
        //   });
        //   models.post.hasMany(models.Comment);



    };

    return Profile;

}
