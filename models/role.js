module.exports = function(sequelize, Sequelize) {

    var Role = sequelize.define('Role', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        roleName: {
            type: Sequelize.ENUM('Manager', 'Staff', 'Project Manager'),
            defaultValue: 'Project Manager',
            allowNull: false
        },





    });

    Role.associate = ((models) => {
        models.Role.hasMany(models.User, {
            onDelete: "CASCADE",
            foreignKey: 'RoleId'
        })

    })

    return Role;

}
