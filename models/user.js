module.exports = function(sequelize, Sequelize) {

    var User = sequelize.define('User', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        firstName: {
            type: Sequelize.STRING,
            allowNull: true
        },

        lastName: {
            type: Sequelize.STRING,
            allowNull: true
        },

        username: {
            type: Sequelize.TEXT,
            allowNull: false,
            validate: {
                len: [4, 50] // must be between 8 and 50.
            }
        },

        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true
            }
        },

        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        current_business: { type: Sequelize.STRING },

        permission: { type: Sequelize.STRING },

        ProfileId: { type: Sequelize.INTEGER, allowNull: false },
        RoleId: { type: Sequelize.INTEGER, allowNull: false },
        DepartmentId: { type: Sequelize.INTEGER, allowNull: false }




    });
    User.associate = function(models) {
        models.User.hasMany(models.Post);
        models.User.belongsTo(models.Profile, {
            onDelete: "CASCADE"
        });
        models.User.belongsTo(models.Role, {
            onDelete: "CASCADE"
        });
        models.User.belongsTo(models.Department, {
            onDelete: "CASCADE"
        });





    };


    return User;

}
