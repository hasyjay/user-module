var express = require('express');
var router = express.Router();


// Require our controllers.
var user_controller = require('../controllers/userController');
var post_controller = require('../controllers/postController');
var department_controller = require('../controllers/departmentController');
var profile_controller = require('../controllers/profileController');
var role_controller = require('../controllers/roleController');


/// POST ROUTES ///

// GET request for creating a Post. NOTE This must come before routes that display Post (uses id).
router.get('/post/create', post_controller.post_create_get);

// POST request for creating Post.
router.post('/post/create', post_controller.post_create_post);

// GET request to delete Post.
router.get('/post/:post_id/delete', post_controller.post_delete_get);

// POST request to delete Post.
router.post('/post/:post_id/delete', post_controller.post_delete_post);

// GET request to update Post.
router.get('/post/:post_id/update', post_controller.post_update_get);

// POST request to update Post.
router.post('/post/:post_id/update', post_controller.post_update_post);

// GET request for one Post.
router.get('/post/:post_id', post_controller.post_detail);

// GET request for list of all Post.
router.get('/posts', post_controller.post_list);

// post by a user route //
router.get('/posts/:user_id/:department_id/:profile_id', post_controller.user_post_dept_profile);
router.get('/posts/:user_id/:department_id', post_controller.user_post_dept);

router.get('/posts/:user_id', user_controller.user_post);


/// role ROUTES ///

// GET request for creating a role. NOTE This must come before route that displays role (uses id).
router.get('/role/create', role_controller.role_create_get);

// POST request for creating role.
router.post('/role/create', role_controller.role_create_post);

// GET request to delete role.
router.get('/role/:role_id/delete', role_controller.role_delete_get);

// POST request to delete role.
router.post('/role/:role_id/delete', role_controller.role_delete_post);

// GET request to update role.
router.get('/role/:role_id/update', role_controller.role_update_get);

// POST request to update role.
router.post('/role/:role_id/update', role_controller.role_update_post);

// GET request for one role.
router.get('/role/:role_id', role_controller.role_detail);

// GET request for list of all roles.
router.get('/roles', role_controller.role_list);


/// user ROUTES ///

// GET request for creating user. NOTE This must come before route for id (i.e. display user).
router.get('/user/create', user_controller.user_create_get);

// POST request for creating user.
router.post('/user/create', user_controller.user_create_post);

// GET request to delete user.
router.get('/user/:user_id/delete', user_controller.user_delete_get);

// POST request to delete user
router.post('/user/:user_id/delete', user_controller.user_delete_post);

// GET request to update user.
router.get('/user/:user_id/update', user_controller.user_update_get);

// POST request to update user.
router.post('/user/:user_id/update', user_controller.user_update_post);

// GET request for one user.
router.get('/user/:user_id', user_controller.user_detail);

// GET request for list of all users.
router.get('/users', user_controller.user_list);


/// department ROUTES ///

// GET request for creating a department. NOTE This must come before route that displays department (uses id).
router.get('/department/create', department_controller.department_create_get);

// POST request for creating department.
router.post('/department/create', department_controller.department_create_post);

// GET request to delete department.
router.get('/department/:department_id/delete', department_controller.department_delete_get);

// POST request to delete department.
router.post('/department/:department_id/delete', department_controller.department_delete_post);

// GET request to update department.
router.get('/department/:department_id/update', department_controller.department_update_get);

// POST request to update department.
router.post('/department/:department_id/update', department_controller.department_update_post);

// GET request for one department.
router.get('/department/:department_id', department_controller.department_detail);

// GET request for list of all departments.
router.get('/departments', department_controller.department_list);


/// profile ROUTES ///

// GET request for creating profile. NOTE This must come before route for id (i.e. display profile).
router.get('/profile/create', profile_controller.profile_create_get);

// POST request for creating profile.
router.post('/profile/create', profile_controller.profile_create_post);

// GET request to delete profile.
router.get('/profile/:profile_id/delete', profile_controller.profile_delete_get);

// POST request to delete profile
router.post('/profile/:profile_id/delete', profile_controller.profile_delete_post);

// GET request to update profile.
router.get('/profile/:profile_id/update', profile_controller.profile_update_get);

// POST request to update profile.
router.post('/profile/:profile_id/update', profile_controller.profile_update_post);

// GET request for one profile.
router.get('/profile/:profile_id', profile_controller.profile_detail);

// GET request for list of all profiles.
router.get('/profiles', profile_controller.profile_list);

// GET blog home page.
router.get('/', post_controller.index);

// export all the router created
module.exports = router;
